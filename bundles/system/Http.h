#pragma once

#include <pistache/router.h>

using HttpRequest = Pistache::Rest::Request;
using HttpResponseWriter = Pistache::Http::ResponseWriter;